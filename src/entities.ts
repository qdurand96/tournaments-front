export interface User {
    id?:number;
    email?:string;
    password?:string;
    username?:string;
    role?:string;
    avatar?:string;
    teamRole?:string;
    status?:number;
    riotAPIId?:number;
    teamId?:number;
}

export interface Team {
    id?:number;
    name?:string;
    avatar?:string;
    win?:number;
    status?:number;
    players?:User[]
}

export interface Tournament {
    id?:number;
    name?:string;
    image?:string;
    date?:string;
    price?:number;
    teamNb?:any;
    cashprize?:number;
    text?:string;
    winnerId?:string;
    status?:number;
    riotAPIId?:number;
    userId?:string;
    teams?:Team[]
}