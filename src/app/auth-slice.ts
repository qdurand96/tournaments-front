import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { User } from '../entities'
import { RootState } from './store'
import { userApi } from './User-api'



export interface AuthState {
    user?: User | null
    token?: string | null
}

export const authSlice = createSlice({
    name: 'auth',
    initialState: { user: null, token: null } as AuthState,
    reducers: {
        setCredentials: (
            state,
            { payload: { user, token } }: PayloadAction<AuthState>
        ) => {
            state.user = user
            state.token = token
            localStorage.setItem('token', String(token))
        },
        logout: (state) => {
            state.user = null
            state.token = null
            localStorage.removeItem('token')
        }
    },
    extraReducers: (builder) => {
        builder.addMatcher(
            userApi.endpoints.getThisUser.matchFulfilled,
            (state, { payload }) => {
                state.user = payload
            }
        )
    }
})

export const { setCredentials,logout } = authSlice.actions

export default authSlice.reducer

export const selectCurrentUser = (state: RootState) => state.auth.user
