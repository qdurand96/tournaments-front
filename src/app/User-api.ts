import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { User } from '../entities';
import { PasswordState } from '../pages/AccountPage';
import { AuthState } from './auth-slice';
import { prepare } from './token-interceptor';



export const userApi = createApi({
    reducerPath: 'userApi',
    tagTypes: ['user'],
    baseQuery: fetchBaseQuery({ baseUrl: process.env.REACT_APP_SERVER_URL + '/api/user', prepareHeaders: prepare }),
    endpoints: (builder) => ({
        getThisUser: builder.query<User, void>({
            query: () => '/',
            providesTags: ['user']
        }),
        getOneUser: builder.query<User, string>({
            query: (id) => '/'+id
        }),
        userLogin: builder.mutation<AuthState, User>({
            query: (body) => ({
                url: '/login',
                method: 'POST',
                body
            }),
            invalidatesTags: ['user']
        }),
        userRegister: builder.mutation<User, User>({
            query: (body) => ({
                url: '/register',
                method: 'POST',
                body
            }),
            invalidatesTags: ['user']
        }),
        userDelete: builder.mutation<void, number>({
            query: (id) => ({
                url: '/'+id,
                method: 'DELETE'
            }),
            invalidatesTags: ['user']
        }),
        userUpdate: builder.mutation<User,FormData>({
            query: (body) => ({
                url: '/',
                method: 'PATCH',
                body
            }),
            invalidatesTags: ['user']
        }),
        passwordUpdate: builder.mutation<void,PasswordState>({
            query: (body) => ({
                url: '/password',
                method: 'PATCH',
                body
            }),
            invalidatesTags: ['user']
        }),
        leaveTeam: builder.mutation<void,void>({
            query: (body) => ({
                url: '/leave',
                method: 'PATCH',
                body
            }),
            invalidatesTags: ['user']
        })
    })
});




export const {
    useGetThisUserQuery,
    useGetOneUserQuery,
    useUserLoginMutation,
    useUserRegisterMutation,
    useUserDeleteMutation,
    useUserUpdateMutation,
    usePasswordUpdateMutation,
    useLeaveTeamMutation
} = userApi
