import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import authSlice from './auth-slice';
import { teamApi } from './Team-api';
import { tournamentApi } from './Tournament-api';
import { userApi } from './User-api';

export const store = configureStore({
  reducer: {
    [teamApi.reducerPath]:teamApi.reducer,
    [tournamentApi.reducerPath]:tournamentApi.reducer,
    [userApi.reducerPath]:userApi.reducer,
    auth:authSlice
  },
  middleware: (getDefaultMiddleware) =>
  getDefaultMiddleware().concat(
    teamApi.middleware,
    tournamentApi.middleware,
    userApi.middleware
  )
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
