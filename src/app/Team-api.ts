import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { Team } from '../entities';
import { prepare } from './token-interceptor';



export const teamApi = createApi({
    reducerPath: 'teamApi',
    tagTypes: ['team','teamList','invitations'],
    baseQuery: fetchBaseQuery({ baseUrl: process.env.REACT_APP_SERVER_URL + '/api/team', prepareHeaders: prepare }),
    endpoints: (builder) => ({
        getLadder: builder.query<Team[], void>({
            query: () => '/',
            providesTags: ['teamList']
        }),
        getOneTeam: builder.query<Team, number>({
            query: (id) => '/'+id,
            providesTags: ['team']
        }),
        createTeam: builder.mutation<Team, FormData>({
            query: (body) => ({
                url: '/',
                method: 'POST',
                body
            }),
            invalidatesTags: ['team','teamList']
        }),
        updateTeam: builder.mutation<Team,FormData>({
            query: (body) => ({
                url: '/',
                method: 'PATCH',
                body
            }),
            invalidatesTags: ['team','teamList']
        }),
        deleteTeam: builder.mutation<void, number>({
            query: () => ({
                url: '/',
                method: 'DELETE'
            }),
            invalidatesTags: ['team','teamList']
        }),
        getInvitations: builder.query<Team[], void>({
            query: () => '/get/invitation',
            providesTags:['invitations']
        }),
        sendInvitation: builder.mutation<any, string>({
            query: (body) => ({
                url: '/post/invitation',
                method: 'POST',
                body
            })
        }),
        acceptInvitation: builder.mutation<string,number>({
            query: (id) => ({
                url: '/invitation/'+id,
                method: 'PATCH'
            }),
            invalidatesTags:['team','invitations']
        }),
        declineInvation: builder.mutation<string, number>({
            query: (id) => ({
                url: '/invitation/'+id,
                method: 'DELETE'
            }),
            invalidatesTags:['invitations']
        }),
        removeFromTeam: builder.mutation<string,number>({
            query: (id) => ({
                url: '/remove/'+id,
                method: 'PATCH'
            })
        }),
    })
});




export const {
    useGetLadderQuery,
    useGetOneTeamQuery,
    useCreateTeamMutation,
    useUpdateTeamMutation,
    useDeleteTeamMutation,
    useGetInvitationsQuery,
    useSendInvitationMutation,
    useAcceptInvitationMutation,
    useDeclineInvationMutation,
    useRemoveFromTeamMutation
} = teamApi
