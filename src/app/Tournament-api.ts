import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { Tournament } from '../entities';
import { prepare } from './token-interceptor';



export const tournamentApi = createApi({
    reducerPath: 'tournamentApi',
    tagTypes: ['tournaments', 'userTournaments','teamTournaments','oneTournament'],
    baseQuery: fetchBaseQuery({ baseUrl: process.env.REACT_APP_SERVER_URL + '/api/tournament', prepareHeaders: prepare }),
    endpoints: (builder) => ({
        getAllTournaments: builder.query<Tournament[], void>({
            query: () => '/',
            providesTags: ['tournaments']
        }),
        getUserTournaments: builder.query<Tournament[], void>({
            query: () => '/user',
            providesTags:['userTournaments']
        }),
        getTeamTournaments: builder.query<Tournament[], void>({
            query: () => '/team',
            providesTags:['teamTournaments']
        }),
        getOneTournament: builder.query<Tournament, string>({
            query: (id) => '/'+id,
            providesTags:['oneTournament']
        }),
        createTournament: builder.mutation<Tournament, FormData>({
            query: (body) => ({
                url: '/',
                method: 'POST',
                body
            }),
            invalidatesTags:['tournaments', 'userTournaments']
        }),
        updateTournament: builder.mutation<Tournament,{id:number, body:FormData}>({
            query: ({id, body}) => ({
                url: '/'+id,
                method: 'PATCH',
                body
            }),
            invalidatesTags:['tournaments', 'userTournaments', 'teamTournaments','oneTournament']
        }),
        deleteTournament: builder.mutation<void, number>({
            query: (id) => ({
                url: '/'+id,
                method: 'DELETE'
            }),
            invalidatesTags:['tournaments', 'userTournaments', 'teamTournaments']
        }),
        endTournament: builder.mutation<string,{id:number, body:number}>({
            query: ({id, body}) => ({
                url: '/end/'+id,
                method: 'PATCH',
                body
            })
        }),
        joinTournament: builder.mutation<string, number>({
            query: (id) => ({
                url: '/registration/'+id,
                method: 'POST'
            }),
            invalidatesTags:['teamTournaments','oneTournament']
        }),
        leaveTournament: builder.mutation<string, number>({
            query: (id) => ({
                url: '/registration/'+id,
                method: 'DELETE'
            }),
            invalidatesTags:['teamTournaments','oneTournament']
        })
    })
});




export const {
    useGetAllTournamentsQuery,
    useGetUserTournamentsQuery,
    useGetTeamTournamentsQuery,
    useGetOneTournamentQuery,
    useCreateTournamentMutation,
    useUpdateTournamentMutation,
    useDeleteTournamentMutation,
    useEndTournamentMutation,
    useJoinTournamentMutation,
    useLeaveTournamentMutation
} = tournamentApi
