import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { useGetThisUserQuery } from './app/User-api';
import { Navbar } from './components/Navbar';
import { HomePage } from './pages/HomePage';
import { CreatePage } from './pages/CreatePage';
import { RegisterPage } from './pages/RegisterPage';
import { LoginPage } from './pages/LoginPage';
import { TournamentsPage } from './pages/TournamentsPage';
import { Footer } from './components/Footer';
import { TeamPage } from './pages/TeamPage';
import { LadderPage } from './pages/LadderPage';
import { OneTournamentPage } from './pages/OneTournamentPage';
import { AccountPage } from './pages/AccountPage';
import { MerciPage } from './pages/MerciPage';
import { OneTeamPage } from './pages/OneTeamPage';

function App() {

  useGetThisUserQuery(undefined, { skip: localStorage.getItem('token') === null });

  return (
    <BrowserRouter>
      <Navbar/>
      <Routes>
        <Route path="/" element={<HomePage/>} />
        <Route path="/tournaments" element={<TournamentsPage/>} />
        <Route path="/tournaments/:id" element={<OneTournamentPage/>} />
        <Route path="/create" element={<CreatePage/>} /> 
        <Route path="/team" element={<TeamPage/>} />
        <Route path="/team/:id" element={<OneTeamPage/>} />
        <Route path="/ladder" element={<LadderPage/>} />
        <Route path="/register" element={<RegisterPage/>} />
        <Route path="/login" element={<LoginPage/>} />
        <Route path="/account" element={<AccountPage/>} />
        <Route path="/merci" element={<MerciPage/>} />
        <Route path="*" element={<HomePage/>} />
      </Routes>
      <Footer/>
    </BrowserRouter>
  );
}

export default App;
