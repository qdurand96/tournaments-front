import './Forms.scss'
import { useState } from "react";
import { useUserLoginMutation, useUserRegisterMutation } from "../app/User-api";
import { User } from "../entities";
import { useAppDispatch } from '../app/hooks';
import { setCredentials } from '../app/auth-slice';

export function RegisterForm() {

    const [registerUser] = useUserRegisterMutation();
    const [form, setForm] = useState<User>({} as User);
    const [postLogin, postQuery] = useUserLoginMutation();
    let dispatch = useAppDispatch()

    const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
        try {
            event.preventDefault();
            await registerUser(form).unwrap();

            const data = await postLogin(
                {
                    email: form.email,
                    password: form.password
                }
            ).unwrap()

            if (data && !postQuery.isError) {
                dispatch(setCredentials(data))
            }

        } catch (error: any) {
            console.log(error.data);
        }
    }

    const handleChange = (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.value
        let change = { ...form, [name]: value }

        setForm(change)
    }

    return (
        <form onSubmit={handleSubmit}>
            <div className="mb-3">
                <label htmlFor="username" className="form-label">Nom d'utilisateur</label>
                <input type="text" className="form-control" id="username" name='username' maxLength={12} onChange={handleChange} />
            </div>
            <div className="mb-3">
                <label htmlFor="email" className="form-label">Email</label>
                <input type="email" className="form-control" id="email" name='email' onChange={handleChange} />
            </div>
            <div className="mb-3">
                <label htmlFor="password" className="form-label">Mot de passe</label>
                <input type="password" className="form-control" id="password" name='password' autoComplete="off" onChange={handleChange} />
            </div>
            <div className="d-flex justify-content-center pt-1">
                <button type="submit" className="btn btnCustom">S'inscrire</button>
            </div>
        </form>
    )
}