import { useState } from 'react'
import { Navigate } from 'react-router-dom'
import { useAppDispatch, useAppSelector } from '../app/hooks'
import { useDeleteTeamMutation, useGetOneTeamQuery, useUpdateTeamMutation } from '../app/Team-api'
import { userApi } from '../app/User-api'
import { Team } from '../entities'
import { AddButton } from './AddButton'
import { PlayerCard } from './PlayerCard'
import './TeamMain.scss'

interface Props {
    id: number;
}

export function TeamMain({ id }: Props) {

    const user = useAppSelector(state => state.auth.user)

    const [open, setOpen] = useState<Boolean>(false)
    const [form, setForm] = useState<Team>({} as Team);

    const { data, isLoading } = useGetOneTeamQuery(id)
    const [updateTeam] = useUpdateTeamMutation()
    const [deleteTeam] = useDeleteTeamMutation()
    let dispatch = useAppDispatch()





    function createCards() {
        let cards: any = []
        for (let index = 0; index < 5; index++) {
            if (data?.players) {
                if (data?.players[index]) {
                    const oneCard = <PlayerCard key={index} player={data.players[index]} />
                    cards.push(oneCard)
                } else if (user?.teamRole === 'admin' && user.teamId === data?.id) {
                    const btn = <AddButton key={index} />
                    cards.push(btn)
                }
            }
        }
        return cards
    }

    async function deleteThis() {
        await deleteTeam(data?.id as number)
        dispatch(userApi.util.invalidateTags(["user"]))
    }

    const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
        try {
            event.preventDefault();

            const formData = new FormData();
            form.name !== undefined && formData.append('name', form.name as string);
            form.avatar !== undefined && formData.append('avatar', form.avatar as string);


            if (data?.id) {
                await updateTeam(formData)
                setOpen(false)
            }

        } catch (error: any) {
            console.log(error.data);
        }
    }

    const handleChange = (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.value
        let change = { ...form, [name]: value }
        setForm(change)

    }

    const handleFile = (event: any) => {
        setForm({
            ...form,
            [event.target.name]: event.target.files[0]
        })
    }



    return (
        <>
            {!data && !isLoading && <Navigate to='/home' />}
            {open ?
                <>
                    <div className="col-2"></div>
                    <div className="col-8">
                        <form onSubmit={handleSubmit}>
                            <div className="mb-3">
                                <label htmlFor="name" className="form-label">Nom</label>
                                <input type="text" className="form-control" id="name" name='name' onChange={handleChange} />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="avatar" className="form-label">Avatar</label>
                                <input type="file" className="form-control" id="avatar" name='avatar' alt='img' onChange={handleFile} />
                            </div>
                            <div className="d-flex justify-content-center pt-1">
                                <button type="submit" className="btn btnCustom updateBtn">Valider</button>
                            </div>
                        </form>
                    </div>
                    <div className="col-2"></div>
                </> :
                <>
                    <div className="col-md-2"></div>
                    <div className="col-md-8 col-12">
                        <div className="row">
                            <div className="col-1 d-flex align-items-center p-0">
                                <div className="line"></div>
                            </div>
                            <div className="col-11 d-flex">
                                <img className="thumbnail" src={data?.avatar ? data?.avatar : '/assets/noIcon.png'} alt='...' />
                                <h5 className="teamName">{data?.name}</h5>
                                {user?.teamRole === 'admin' && user.teamId === data?.id &&
                                    <>
                                        <div onClick={() => setOpen(true)} className="mx-3 align-self-center text-center updateProfile">
                                            <i className="fas fa-pencil-alt"></i>
                                        </div>
                                        <div onClick={() => deleteThis()} className=" align-self-center text-center updateProfile">
                                            <i className="fas fa-trash-alt"></i>
                                        </div>
                                    </>
                                }
                            </div>
                        </div>
                        <div className="row justify-content-between">
                            {createCards()}
                        </div>
                    </div>
                    <div className="col-md-2"></div>
                </>
            }
        </>
    )
}
