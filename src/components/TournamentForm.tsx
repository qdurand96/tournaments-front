import { useState } from 'react';
import { useCreateTournamentMutation } from '../app/Tournament-api';
import { Tournament } from '../entities';
import './Forms.scss'

interface Props {
    close:any
}

export function TournamentForm({close}:Props) {

    const [form, setForm] = useState<Tournament>({} as Tournament);
    const [postTournament] = useCreateTournamentMutation();

    /**
     * Post the form to the server
     * @param event 
     */
    const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
        try {
            event.preventDefault();

            const formData = new FormData();
            formData.append('name', form.name as string);
            formData.append('image', form.image as string);
            formData.append('date', form.date as string);
            formData.append('teamNb', form.teamNb as string);
            formData.append('text', form.text as string);


            await postTournament(formData)
            close()

        } catch (error: any) {
            console.log(error.data);
        }
    }

    const handleChange = (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.value
        let change = { ...form, [name]: value }
        setForm(change)
    }

    const handleFile = (event: any) => {
        setForm({
            ...form,
            [event.target.name]: event.target.files[0]
        })
    }
    


    return (
        <>
            <div className="col-2"></div>
            <div className="col-8">
                <form onSubmit={handleSubmit}>
                    <div className="mb-3">
                        <label htmlFor="name" className="form-label">Nom du tournois</label>
                        <input type="text" className="form-control" id="name" name='name' onChange={handleChange} />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="image" className="form-label">Image</label>
                        <input type="file" className="form-control" id="image" name='image' alt='img' onChange={handleFile} />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="date" className="form-label">Date</label>
                        <input type="date" className="form-control" id="date" name='date' onChange={handleChange} />
                    </div>
                    {/* <div className="mb-3">
                <label htmlFor="number" className="form-label">Prix</label>
                <input type="number" className="form-control" id="number" name='number' onChange={handleChange}/>
            </div> */}
                    <div className="mb-3">
                        <label htmlFor="teamNb" className="form-label">Nombre d'equipes</label> <br />
                        <div className="form-check form-check-inline">
                            <input className="form-check-input" type="radio" name="teamNb" id="inlineRadio1" value={4} onChange={handleChange} />
                            <label className="form-check-label" htmlFor="inlineRadio1">4</label>
                        </div>
                        <div className="form-check form-check-inline">
                            <input className="form-check-input" type="radio" name="teamNb" id="inlineRadio2" value={8} onChange={handleChange} />
                            <label className="form-check-label" htmlFor="inlineRadio2">8</label>
                        </div>
                        <div className="form-check form-check-inline">
                            <input className="form-check-input" type="radio" name="teamNb" id="inlineRadio3" value={16} onChange={handleChange} />
                            <label className="form-check-label" htmlFor="inlineRadio3">16</label>
                        </div>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="text" className="form-label">Presentation</label>
                        <textarea className="form-control" id="text" name='text' onChange={handleChange} rows={3}></textarea>
                    </div>
                    <div className="d-flex justify-content-center pt-1">
                        <button type="submit" className="btn btnCustom">Creer</button>
                    </div>
                </form>
            </div>
            <div className="col-2"></div>
        </>
    )
}