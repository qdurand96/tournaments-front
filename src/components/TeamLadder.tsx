import { useNavigate } from 'react-router-dom'
import { Team } from '../entities'
import './TeamLadder.scss'

interface Props {
    rank:number,
    team:Team
}

export function TeamLadder({rank,team}:Props) {

let navigate = useNavigate()

    return (
        <>

            <div className="col-sm-2 col-0"></div>
            <div className="col-sm-8 col-12">
                <div onClick={()=>navigate('/team/'+team.id)} className="my-1 oneTeam">
                    <div className="sideDiv">
                        <div >{rank}</div>
                        <div className="vl"></div>
                        <div className="space"></div>
                        <div>{team.name}</div>
                    </div>
                    <div className="sideDiv">
                        <img className="thumbnail" src={team.avatar ? team.avatar : '/assets/noIcon.png'} alt='...' />
                        <div className='points'>{team.win}</div>
                    </div>
                </div>
            </div>
            <div className="col-sm-2 col-0"></div>
        </>
    )
}
