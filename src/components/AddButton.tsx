import { useState } from 'react'
import { useSendInvitationMutation } from '../app/Team-api';
import './TeamMain.scss'

export function AddButton() {

    const [open, setOpen] = useState<Boolean>(false)
    const [form, setForm] = useState<any>({});
    const [sendInvitation, result] = useSendInvitationMutation()

    const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
        try {
            event.preventDefault();

            await sendInvitation(form)

        } catch (error: any) {
            console.log(error.data);
        }
    }

    const handleChange = (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.value
        let change = { ...form, [name]: value }
        setForm(change)
    }

    return (
        <>
            {!open ?
                <div className="col-md-2 col-5 d-flex justify-content-center align-items-center text-center" >
                    <div onClick={() => setOpen(true)} className="btnAdd">
                        <i className="fas fa-plus"></i>
                    </div>
                </div>
                :
                <form onSubmit={handleSubmit} className="col-md-2 col-5 my-3 p-0  d-flex flex-column justify-content-center" >
                    {!result.isSuccess &&
                        <input type="text" autoComplete='off' className="form-control addInput py-0" id="username" name='invitedUser' placeholder='ID du joueur' maxLength={12} onChange={handleChange} />
                    }
                    {result.isError &&
                        <p className="text-center feedback">Utilisateur introuvable</p>
                    }
                    {result.isSuccess &&
                        <p className="text-center feedback">Utilisateur ajouté</p>
                    }
                </form>
                // <form onSubmit={handleSubmit} className="col-md-2 col-5 p-0 d-flex flex-column justify-content-between" >
                //     <div className="d-flex justify-content-center w-100 my-3" >
                //         <img src={'/assets/noIcon.png'} className=" addPlayer w-75" alt="..." />
                //     </div>
                //     <input  type="text" className="form-control addInput py-0" id="username" name='invitedUser' placeholder='ID du joueur' maxLength={12} onChange={handleChange}/>
                // </form>
            }
        </>
    )
}
