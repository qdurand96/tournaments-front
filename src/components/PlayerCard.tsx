import { User } from '../entities'
import './TeamMain.scss'

interface Props {
    player: User
}

export function PlayerCard({ player }: Props) {

    return (
        <>
            {/* <div className="card bg-dark col-md-2 col-5 cardPlayer p-0" >
                <img src={player.avatar ? player.avatar : '/assets/noIcon.png'} className="card-img" alt="..." />
                <div className="card-img-overlay overlay">
                    <h6 className="card-title">{player.username}</h6>
                </div>
            </div> */}
            <div className="col-md-2 col-5 p-0 d-flex flex-column justify-content-between" >
                <div className="d-flex justify-content-center w-100 my-3" >
                    <img src={player.avatar ? player.avatar : '/assets/noIcon.png'} className=" imgPlayer w-75" alt="..." />
                </div>
                <h6 className="username">{player.username}</h6>
            </div>
        </>
    )
}
