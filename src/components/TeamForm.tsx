import { useState } from 'react';
import { useCreateTeamMutation } from '../app/Team-api';
import { Team } from '../entities';
import './Forms.scss'

export function TeamForm() {

    const [open, setOpen] = useState<Boolean>(false)
    const [form, setForm] = useState<Team>({} as Team);
    const [postTeam] = useCreateTeamMutation()

    /**
     * Post the form to the server
     * @param event 
     */
    const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
        try {
            event.preventDefault();

            const formData = new FormData();
            formData.append('name', form.name as string);
            formData.append('avatar', form.avatar as string);

            await postTeam(formData)
            window.location.reload();
            // navigate("/team")
        } catch (error: any) {
            console.log(error.data);
        }
    }

    const handleChange = (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.value
        let change = { ...form, [name]: value }
        setForm(change)
    }

    const handleFile = (event: any) => {
        setForm({
            ...form,
            [event.target.name]: event.target.files[0]
        })
    }



    return (
        <>
            {open ?
                <>
                    <div className="col-2"></div>
                    <div className="col-8">
                        <form onSubmit={handleSubmit}>
                            <div className="mb-3">
                                <label htmlFor="name" className="form-label">Nom de l'équipe</label>
                                <input type="text" className="form-control" id="name" name='name' maxLength={12} onChange={handleChange} />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="avatar" className="form-label">Avatar</label>
                                <input type="file" className="form-control" id="avatar" name='avatar' alt='img' onChange={handleFile} />
                            </div>
                            <div className="d-flex justify-content-center pt-1">
                                <button type="submit" className="btn btnCustom">Creer</button>
                            </div>
                        </form>
                    </div>
                    <div className="col-2"></div>
                </>
                :
                <>
                    <div className="col-md-3 col-1"></div>
                    <div className="col-md-6 col-10 text-center">
                        <div>
                            <button onClick={() => setOpen(true)} className="btn btnCustom w-50 my-5">CREER UNE <br /> EQUIPE</button>
                        </div>
                    </div>
                    <div className="col-md-3 col-1"></div>
                </>}

        </>
    )
}