import { useAcceptInvitationMutation, useDeclineInvationMutation } from "../app/Team-api";
import { Team } from "../entities";
import './Invitation.scss'

interface Props {
    team: Team
}

export function Invitation({ team }: Props) {

    const [accept] = useAcceptInvitationMutation();
    const [decline] = useDeclineInvationMutation();
    

    return (
        <>
            <div className="dropdown-item invitations">
                <div className="mx-2">{team.name}</div>
                <div className="d-flex">
                    <div onClick={() => team.id && accept(team.id)} className="text-center btnAccept m-2">
                        <i className="fas fa-check"></i>
                    </div>
                    <div onClick={() => team.id && decline(team.id)} className="text-center btnRefuse m-2">
                        <i className="fas fa-xmark"></i>
                    </div>
                    {/* <button className="btnAccept">Accepter</button>
                    <button className="btnRefuse">Refuser</button> */}
                </div>
            </div>
        </>
    )
}
