import './TournamentCard.scss'
import { Tournament } from '../entities';
import { Link } from 'react-router-dom';

interface Props {
    tournament: Tournament
}

export function TournamentCard({ tournament }: Props) {
    return (
        <Link to={"/tournaments/"+tournament.id} className="card bg-dark col-md-3 col-sm-5 col-10 " >
            <img src={tournament.image ? tournament.image : '/assets/lolLogo.jpg'} className="card-img" alt="..." />
            <div className="card-img-overlay overlay">
                <p>{new Intl.DateTimeFormat('fr-FR').format(new Date(String(tournament.date)))}</p>
                <p>{tournament.name}</p>
            </div>
        </Link>
    )
}