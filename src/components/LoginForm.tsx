import { useState } from 'react';
import { setCredentials } from '../app/auth-slice';
import { useAppDispatch } from '../app/hooks';
import { useUserLoginMutation } from '../app/User-api';
import { User } from '../entities';
import './Forms.scss'

export function LoginForm() {
    const [postLogin, postQuery] = useUserLoginMutation();
    const [form, setForm] = useState<User>({} as User);
    let dispatch = useAppDispatch()
    /**
     * Post the form to the server
     * @param event 
     */
    const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
        try {
            event.preventDefault();
            
            const data = await postLogin(form).unwrap()

            if (data && !postQuery.isError) {
                dispatch(setCredentials(data))
            }
        } catch (error: any) {
            console.log(error.data);

        }

    }

    const handleChange = (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.value
        let change = { ...form, [name]: value }


        setForm(change)
    }

    return (
        <form onSubmit={handleSubmit}>
            <div className="mb-3">
                <label htmlFor="email" className="form-label">Email</label>
                <input type="email" className="form-control" id="email" name='email' onChange={handleChange} />
            </div>
            <div className="mb-3">
                <label htmlFor="password" className="form-label">Mot de passe</label>
                <input type="password" className="form-control" id="password" name='password' autoComplete="off" onChange={handleChange}/>
            </div>
            <div className="d-flex justify-content-center pt-1">
                <button type="submit" className="btn btnCustom">Se connecter</button>
            </div>
        </form>
    )
}