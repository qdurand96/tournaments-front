import { Link } from 'react-router-dom'
import './Footer.scss'

export function Footer() {

    
    return (
        <div className="container-fluid footer text-center">
            <div className="row mt-2">
                {/* <p>Copyright Dudu ♥</p> */}
                <Link to="/merci">Copyright Dudu ♥</Link>
            </div>
        </div>
    )
}