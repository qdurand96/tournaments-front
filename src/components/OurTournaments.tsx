
import { useGetTeamTournamentsQuery } from '../app/Tournament-api';
import { Tournament } from '../entities';
import './TeamMain.scss'
import { TournamentsDropdown } from './TournamentsDropdown'

export function OurTournaments() {

    const { data } = useGetTeamTournamentsQuery()

    const pastTournaments = data?.filter(x => Date.parse(x.date as string) < Number(new Date()));
    const futureTournaments = data?.filter(x => Date.parse(x.date as string) > Number(new Date()));

    return (
        <>

            <div className="col-3"></div>
            <div className="col-6 text-center mt-3" style={{ color: "#c8bb8f" }}>
                <h5>NOS TOURNOIS</h5>
                <img src={'/assets/roundLine.png'} className="w-25" alt="" />
            </div>
            <div className="col-3"></div>

            <div className="col-md-2"></div>
            <div className="col-md-8 col-12">
                <TournamentsDropdown tournaments={futureTournaments as Tournament[]} passed={false} />
                <TournamentsDropdown tournaments={pastTournaments as Tournament[]} passed={true} />
            </div>
            <div className="col-md-2"></div>
        </>
    )
}
