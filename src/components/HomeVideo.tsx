import './HomeVideo.scss'

export function HomeVideo() {

    return (
        <>
            <div className="containerVid w-100">
                <video autoPlay={true} playsInline={true} loop={true} muted={true} className='w-100'>
                    <source src="/assets/bannerVid.mp4" type="video/mp4" />
                </video>
                <div className="overlayVid ">
                    GOOD LUCK <br />
                    HAVE FUN
                </div>
            </div>
        </>

    )
}
