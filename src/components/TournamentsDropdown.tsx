import { useState } from 'react'
import { Tournament } from '../entities'
import './TeamMain.scss'

interface Props {
    tournaments: Tournament[];
    passed: boolean;
}

export function TournamentsDropdown({ tournaments, passed }: Props) {

    const [open, setOpen] = useState(false)


    // function useDelayUnmount(isMounted: any, delayTime: any) {
    //     const [showDiv, setShowDiv] = useState(false);
    //     useEffect(() => {
    //         let timeoutId: any;
    //         if (isMounted && !showDiv) {
    //             setShowDiv(true);
    //         } else if (!isMounted && showDiv) {
    //             timeoutId = setTimeout(() => setShowDiv(false), delayTime); //delay our unmount
    //         }
    //         return () => clearTimeout(timeoutId); // cleanup mechanism for effects , the use of setTimeout generate a sideEffect
    //     }, [isMounted, delayTime, showDiv]);
    //     return showDiv;
    // }

    // const showDiv = useDelayUnmount(open, 450);
    // const mountedStyle = { animation: "inAnimation 450ms ease-in" };
    // const unmountedStyle = {
    //     animation: "outAnimation 470ms ease-out",
    //     animationFillMode: "forwards"
    // };

    function createDropdown() {
        return (
            tournaments!==undefined && tournaments.length > 0  ?
                tournaments.map(item =>
                    <div key={item.id} className="my-3 dropdownItem">
                        <div>{item.name}</div>
                        <div>{new Intl.DateTimeFormat('fr-FR').format(new Date(String(item.date)))}</div>
                    </div>
                ) :
                <div className="my-3 noTournament">
                    <div>Pas de tournoi</div>
                </div>
        )
    }

    return (
        <>
            <div>
                <div className={open ? "my-3 dropdownStyle activeDd" : "my-3 dropdownStyle"} onClick={() => setOpen(!open)}>
                    <div className='dd'> {passed ? 'TOURNOIS PASSÉS' : 'TOURNOIS A VENIR'} </div>
                    <i className="fas fa-chevron-down"></i>
                </div>
                {open &&
                    createDropdown()
                    // tournaments.map(item =>
                    //     <div key={item.id} className="my-3 dropdownItem">
                    //         <div>{item.name}</div>
                    //         <div>{new Intl.DateTimeFormat('fr-FR').format(new Date(String(item.date)))}</div>
                    //     </div>
                    // )
                    // tournaments.length > 0 ?
                    // tournaments.map(item =>
                    //     <div key={item.id} className="my-3 dropdownItem">
                    //         <div>{item.name}</div>
                    //         <div>{new Intl.DateTimeFormat('fr-FR').format(new Date(String(item.date)))}</div>
                    //     </div>
                    // ) :
                    // <div className="my-3 dropdownItem">
                    //     <div>Pas de tournois</div>
                    // </div>
                }
            </div>


            {/* <div>
                <div className={open ? "my-3 dropdownStyle activeDd" : "my-3 dropdownStyle"} onClick={() => setOpen(!open)}>
                    <div> {passed ? 'TOURNOIS PASSÉS' : 'TOURNOIS A VENIR'} </div>
                    <i className="fas fa-chevron-down"></i>
                </div>
                {showDiv &&
                    tournaments.map(item =>
                        <div className={"my-3 dropdownItem "} style={open ? mountedStyle : unmountedStyle}>
                            <div>{item.name}</div>
                            <div>{new Intl.DateTimeFormat('fr-FR').format(new Date(String(item.date)))}</div>
                        </div>
                    )
                }
            </div> */}
        </>
    )
}
