import './Navbar.scss'
import { useAppDispatch, useAppSelector } from "../app/hooks"
import { Link } from 'react-router-dom'
import { logout } from '../app/auth-slice';
import { useState } from 'react';
import { Logo } from './Logo';
import { useGetInvitationsQuery } from '../app/Team-api';
import { Invitation } from './Invitation';

export function Navbar() {

    const { data } = useGetInvitationsQuery()
    


    const [active, setActive] = useState(false);

    const user = useAppSelector(state => state.auth.user)
    let dispatch = useAppDispatch();

    function handleLogout() {
        dispatch(logout())
    }

    function toggleClass() {
        const currentState = active;
        setActive(!currentState);
    };
    return (
        <nav className="navbar navbar-expand-lg navbarCss p-0">
            <div className="container-fluid">
                <Link className="navbar-brand" to='/'> <Logo /> </Link>
                <button className={active ? 'toggler navbar-toggler active' : 'toggler navbar-toggler'} onClick={toggleClass} type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 200 200">
                        <g strokeWidth="6.5" strokeLinecap="round">
                            <path
                                d="M72 82.286h28.75"
                                fill="#c8aa6e"
                                fillRule="evenodd"
                                stroke="#c8aa6e"
                            />
                            <path
                                d="M100.75 103.714l72.482-.143c.043 39.398-32.284 71.434-72.16 71.434-39.878 0-72.204-32.036-72.204-71.554"
                                fill="none"
                                stroke="#c8aa6e"
                            />
                            <path
                                d="M72 125.143h28.75"
                                fill="#c8aa6e"
                                fillRule="evenodd"
                                stroke="#c8aa6e"
                            />
                            <path
                                d="M100.75 103.714l-71.908-.143c.026-39.638 32.352-71.674 72.23-71.674 39.876 0 72.203 32.036 72.203 71.554"
                                fill="none"
                                stroke="#c8aa6e"
                            />
                            <path
                                d="M100.75 82.286h28.75"
                                fill="#c8aa6e"
                                fillRule="evenodd"
                                stroke="#c8aa6e"
                            />
                            <path
                                d="M100.75 125.143h28.75"
                                fill="#c8aa6e"
                                fillRule="evenodd"
                                stroke="#c8aa6e"
                            />
                        </g>
                    </svg>
                </button>

                <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div className="navbar-nav w-100">
                        <Link className="nav-link" to='/tournaments'>TOURNOIS</Link>
                        <Link className="nav-link" to='/create'>CREER</Link>
                        <Link className="nav-link" to='/team'>EQUIPE</Link>
                        <Link className="nav-link me-auto" to='/ladder'>LADDER</Link>

                        {user ?
                            <div className="nav-item dropdown dropdownSize">
                                <div className="user nav-link dropdown-toggle text-lg-end" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    {data && <i className="fas fa-bell fa-shake notif"></i>}
                                    <img src={user.avatar ? user.avatar : '/assets/noIcon.png'} className='thumbnail' alt="" />
                                    {user.username}
                                </div>
                                <div className="dropdown-menu dropdown-menu-end dropdownMenu" aria-labelledby="navbarDropdown">
                                    {data &&
                                        <>
                                            <div className="invit">Invitations :</div>
                                            {data?.map((item, index) => <Invitation key={index} team={item} />)}
                                        </>
                                    }

                                    <Link className="dropdown-item" to='/account'>Profil</Link>
                                    <button className="dropdown-item" onClick={handleLogout}>Deconnexion</button>
                                </div>
                            </div> :
                            <Link className="nav-link" to='/login'>Connexion</Link>
                        }

                    </div>
                </div>
            </div>
        </nav>
    )
}