import './Header.scss'

interface Props {
    page:string
}

export function Header({page}:Props) {

    return (
        <>
            <div className="col-md-3 col-1"></div>
            <div className="col-md-6 col-10 text-center">
                <div className="my-4">
                    <img src={'/assets/roundLine.png'} className="w-50 upLine" alt="" />
                    <h1 className="my-2">{page}</h1>
                    <img src={'/assets/roundLine.png'} className="w-50" alt="" />
                </div>

            </div>
            <div className="col-md-3 col-1"></div>
        </>
    )
}