import { Navigate } from "react-router-dom";
import { useAppSelector } from "../app/hooks";
import { RegisterForm } from "../components/RegisterForm";

export function RegisterPage() {
    const user = useAppSelector(state => state.auth.user)

    return (
        !user ?
        <div className="container-fluid page">
            <div className="row pt-5">
                <div className="col-3"></div>
                <div className="col-6">
                    <RegisterForm/>
                </div>
                <div className="col-3"></div>
            </div>
        </div>
        :
        <Navigate to='/'/>
    )
}