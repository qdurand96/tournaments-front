import { Link } from "react-router-dom";
import { Navigate } from "react-router-dom";
import { useAppSelector } from "../app/hooks";
import { LoginForm } from "../components/LoginForm";

export function LoginPage() {
    const user = useAppSelector(state => state.auth.user)


    return (
        !user ?
        <div className="container-fluid page">
            <div className="row pt-5">
                <div className="col-3"></div>
                <div className="col-6">
                    <LoginForm/>
                </div>
                <div className="col-3"></div>
            </div>
            <div className="row pt-5">
            <div className="col-3"></div>
                <div className="col-6 text-center">
                    <Link to="/register">Pas de compte? Inscrivez vous!</Link>
                </div>
                <div className="col-3"></div>
            </div>
        </div>
        :
        <Navigate to='/'/>
    )
}