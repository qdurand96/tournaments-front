import { Navigate } from "react-router-dom";
import { useAppSelector } from "../app/hooks";
import { Header } from "../components/Header";
import { OurTournaments } from "../components/OurTournaments";
import { TeamForm } from "../components/TeamForm";
import { TeamMain } from "../components/TeamMain";

export function TeamPage() {

    const user = useAppSelector(state => state.auth.user)
    
    return (
        <>
        {!user && <Navigate to='/login' />}
        <div className="container-fluid page">
            <div className="row">
                <Header page='MON EQUIPE' />
                {user?.teamId ?
                    <>
                        <TeamMain id={user?.teamId}/>
                        <OurTournaments />
                    </>
                    :
                    <TeamForm />
                }
            </div>
        </div>
        </>
    )
}
