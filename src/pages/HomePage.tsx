import { useGetAllTournamentsQuery } from "../app/Tournament-api";
import { HomeVideo } from "../components/HomeVideo";
import { TournamentCard } from "../components/TournamentCard";

export function HomePage() {

    const { data } = useGetAllTournamentsQuery()

    const futureTournaments = data?.filter(x => Date.parse(x.date as string) > Number(new Date()));


    return (
        <>
            <HomeVideo />
            <div className="container-fluid">
                <div className="row justify-content-around">
                    <div className="col-md-3 col-1"></div>
                    <div className="col-md-6 col-10 text-center mt-3" style={{ color: "#c8bb8f" }}>
                        <h5>TOURNOIS A VENIR</h5>
                        <img src={'/assets/roundLine.png'} className="w-25" alt="" />
                    </div>
                    <div className="col-md-3 col-1"></div>
                    {/* {data?.map((item,index) => index<3 && <TournamentCard key={item.id} tournament={item} />)} */}
                    {futureTournaments?.map((item,index) => index>futureTournaments.length-4 && <TournamentCard key={item.id} tournament={item} />)}
                </div>
            </div>
        </>


    )
}
