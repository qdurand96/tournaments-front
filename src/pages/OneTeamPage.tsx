import { useParams } from "react-router-dom";
import { Header } from "../components/Header"
import { TeamMain } from "../components/TeamMain";

export function OneTeamPage() {

    const { id } = useParams<any>();

    return (
        <>
        <div className="container-fluid page">
            <div className="row">
                <Header page='FICHE EQUIPE' />
                <TeamMain id={Number(id)}/>

            </div>
        </div>
        </>
    )
}
