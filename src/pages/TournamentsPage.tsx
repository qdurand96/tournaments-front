import { useGetAllTournamentsQuery } from "../app/Tournament-api";
import { Header } from "../components/Header";
import { TournamentCard } from "../components/TournamentCard";

export function TournamentsPage() {
    const {data} = useGetAllTournamentsQuery()

    return (
        <div className="container-fluid page">
            <div className="row justify-content-around">
                <Header page='TOUS LES TOURNOIS'/>
                {data?.map(item=><TournamentCard key={item.id} tournament={item} />)}
            </div>
        </div>
    )
}