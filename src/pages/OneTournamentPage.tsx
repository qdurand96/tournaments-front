import './Pages.scss'
import { Navigate, useNavigate, useParams } from "react-router-dom";
import { useDeleteTournamentMutation, useGetOneTournamentQuery, useJoinTournamentMutation, useLeaveTournamentMutation, useUpdateTournamentMutation } from "../app/Tournament-api";
import { useGetOneUserQuery } from "../app/User-api";
import { useAppSelector } from '../app/hooks';
import { useEffect, useState } from 'react';
import { Tournament } from '../entities';
import { useGetOneTeamQuery } from '../app/Team-api';

export function OneTournamentPage() {

    const user = useAppSelector(state => state.auth.user)

    const { id } = useParams<any>();
    const { data, isLoading } = useGetOneTournamentQuery(id as string)
    const admin = useGetOneUserQuery(data?.userId as string)
    const userTeam = useGetOneTeamQuery(Number(user?.teamId))
    const [deleteTournament] = useDeleteTournamentMutation()
    const [updateTournament] = useUpdateTournamentMutation()
    const [joinTournament] = useJoinTournamentMutation()
    const [leaveTournament] = useLeaveTournamentMutation()
    const [open, setOpen] = useState<Boolean>(false)
    const [isRegistered, setIsRegistered] = useState<Boolean>()
    const [form, setForm] = useState<Tournament>({} as Tournament);
    let navigate = useNavigate()

    useEffect(() => {

        if (user?.teamRole === "admin" && data && data.teams) {
            for (const team of data.teams) {
                if (team.id === user?.teamId) {
                    setIsRegistered(true)
                } else {
                    setIsRegistered(false)
                }
            }
        }

    }, [data, user?.teamId, user?.teamRole, userTeam.data?.players?.length])

    function deleteThis() {
        deleteTournament(data?.id as number)

        navigate('/create')
    }

    const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
        try {
            event.preventDefault();

            const formData = new FormData();
            form.name !== undefined && formData.append('name', form.name as string);
            form.image !== undefined && formData.append('image', form.image as string);
            form.text !== undefined && formData.append('text', form.text as string);


            if (data?.id) {
                await updateTournament({ id: data?.id, body: formData })
                setOpen(false)
            }

        } catch (error: any) {
            console.log(error.data);
        }
    }

    const handleChange = (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.value
        let change = { ...form, [name]: value }
        setForm(change)
    }

    const handleFile = (event: any) => {
        setForm({
            ...form,
            [event.target.name]: event.target.files[0]
        })
    }

    function leave() {
        leaveTournament(Number(data?.id)) 
        setIsRegistered(false)
    }
    function join () {
        joinTournament(Number(data?.id))
        setIsRegistered(true)
    }


    return (
        <>
            {!data && !isLoading && <Navigate to='/home' />}
            <div className="container-fluid my-5 overflow-hidden page">
                <div className="row">
                    <div className="col-md-1"></div>
                    <form className="col-md-8" onSubmit={handleSubmit}>
                        <div className="row">
                            <div className="col-md-6 order-md-1 order-2">
                                <img src={data?.image ? data?.image : '/assets/lolLogo.jpg'} className="w-100" alt="" />
                                {open && <input type="file" className="form-control" id="image" name='image' alt='img' onChange={handleFile} />}
                                <p className="textTournament fw-bold">Administrateur: <br />{admin.data?.username}</p>
                            </div>
                            <div className="col-md-6 order-md-2 order-1">
                                {open ?
                                    <input type="text" className="form-control mb-3" id="name" name='name' placeholder={data?.name} onChange={handleChange} /> :
                                    <h1 className="tournamentName">{data?.name}</h1>
                                }
                                <h4>{data?.teams?.length}/{data?.teamNb} equipes inscrites</h4>
                                {open ?
                                    <>
                                        <textarea className="form-control" id="text" name='text' placeholder={data?.text} rows={3} onChange={handleChange}></textarea>
                                        <div className="d-flex justify-content-center">
                                            <div onClick={handleSubmit} className="text-center btnTournament m-2">
                                                <i className="fas fa-check"></i>
                                            </div>
                                        </div>
                                    </> :
                                    <p className="textTournament">{data?.text}</p>
                                }
                            </div>
                        </div>
                    </form>

                    <div className="col-md-3 px-0">
                        <div className="date d-flex">
                            <i className="align-self-center fa-regular fa-clock mx-md-3 margin-mobile"></i>
                            {data?.date &&
                                <div className="m-0">{new Intl.DateTimeFormat('fr-FR').format(new Date(String(data?.date)))}</div>
                            }
                        </div>
                        {user?.id === data?.userId &&
                            <div className="text-center d-flex justify-content-md-start justify-content-center my-3">
                                <div onClick={() => setOpen(!open)} className="btnTournament m-2">
                                    <i className="fas fa-pencil-alt"></i>
                                </div>
                                <div onClick={() => deleteThis()} className="btnTournament m-2">
                                    <i className="fas fa-trash-alt"></i>
                                </div>
                            </div>
                        }
                        {userTeam.data?.players?.length === 5 &&
                            <div className=" d-flex justify-content-md-start justify-content-center my-3">
                                {isRegistered ?
                                    <button className="btn btnInscription w-md-50" onClick={() => leave()}>QUITTER LE TOURNOIS</button> :
                                    <button className="btn btnInscription w-md-50" onClick={() => join()}>PARTICIPER AU TOURNOIS</button>
                                }
                            </div>
                        }
                    </div>

                    <div className="col-md-2"></div>
                    <div className="col-md-8">
                        <div className="row">
                            <p className="textTournament fw-bold">Liste des équipes : </p>
                            {data?.teams?.length === 0 ?
                                <p className="textTournament">Pas d'équipe inscrites pour l'instant</p> :
                                data?.teams?.map(item =>
                                    <div key={item.id} className="col-md-4 d-flex justify-content-between">
                                        <p>• {item.name}</p>
                                    </div>
                                )
                            }
                        </div>

                    </div>
                    <div className="col-md-2"></div>
                </div>
            </div>
        </>
    )
}