import { useState } from "react"
import { Navigate } from "react-router-dom"
import { useAppSelector } from "../app/hooks"
import { useGetUserTournamentsQuery } from "../app/Tournament-api"
import { Header } from "../components/Header"
import { TournamentCard } from "../components/TournamentCard"
import { TournamentForm } from "../components/TournamentForm"

export function CreatePage() {
    const user = useAppSelector(state => state.auth.user)
    const { data } = useGetUserTournamentsQuery()

    const [open, setOpen] = useState<Boolean>(false)

    return (
        <>
            {!user && <Navigate to='/login' />}
            <div className="container-fluid page">
                <div className="row justify-content-around">
                    <Header page='MES TOURNOIS' />
                    {open ?
                        <TournamentForm close={() => setOpen(false)} />
                        :
                        <>
                            <div className="col-md-3 col-1"></div>
                            <div className="col-md-6 col-10 text-center">
                                <div>
                                    <button onClick={() => setOpen(true)} className="btn btnCustom w-50 my-5">CREER UN <br /> TOURNOI</button>
                                </div>
                            </div>
                            <div className="col-md-3 col-1"></div>
                        </>
                    }
                    <div className="col-md-3 col-1"></div>
                    <div className="col-md-6 col-10 text-center mt-3" style={{ color: "#c8bb8f" }}>
                        <h5>MES TOURNOIS PRECEDENTS</h5>
                        <img src={'/assets/roundLine.png'} className="w-25" alt="" />
                    </div>
                    <div className="col-md-3 col-1"></div>
                    {data?.map(item => <TournamentCard key={item.id} tournament={item} />)}
                </div>
            </div>
        </>
    )
}