import { useGetLadderQuery } from "../app/Team-api";
import { Header } from "../components/Header";
import { TeamLadder } from "../components/TeamLadder";

export function LadderPage() {

    const { data } = useGetLadderQuery()

    return (
        <div className="container-fluid page">
            <div className="row">
                <Header page='LADDER' />
                {data?.map((item, index) => <TeamLadder key={item.id} rank={index+1} team={item} />)}
            </div>
        </div>
    )
}
