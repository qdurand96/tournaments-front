import { useState } from "react"
import { Navigate } from "react-router-dom"
import { setCredentials } from "../app/auth-slice"
import { useAppDispatch, useAppSelector } from "../app/hooks"
import { usePasswordUpdateMutation, useUserLoginMutation, useUserUpdateMutation } from "../app/User-api"
import { TeamMain } from "../components/TeamMain"
import { User } from "../entities"

export interface PasswordState {
    oldPassword?: string
    newPassword?: string
}

export function AccountPage() {

    const user = useAppSelector(state => state.auth.user)

    const [updateOpen, setUpdateOpen] = useState<Boolean>(false)
    const [passwordOpen, setPasswordOpen] = useState<Boolean>(false)
    const [form, setForm] = useState<User>({} as User);
    const [passwordForm, setPasswordForm] = useState<PasswordState>({} as PasswordState);
    const [updateUser, updateQuery] = useUserUpdateMutation()
    const [updatePassword] = usePasswordUpdateMutation()
    const [postLogin, postQuery] = useUserLoginMutation();
    let dispatch = useAppDispatch()




    const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
        try {
            event.preventDefault();

            const formData = new FormData();
            form.username !== undefined && formData.append('username', form.username as string);
            form.email !== undefined && formData.append('email', form.email as string);
            form.avatar !== undefined && formData.append('avatar', form.avatar as string);
            form.password !== undefined && formData.append('password', form.password as string);


            await updateUser(formData)


            if (!updateQuery.isError && updateQuery.data?.email) {
                const data = await postLogin(
                    {
                        email: updateQuery.data?.email,
                        password: form.password
                    }
                ).unwrap()


                if (data && !postQuery.isError) {
                    dispatch(setCredentials(data))
                }
                setUpdateOpen(false)
            }

        } catch (error: any) {
            console.log(error.data);
        }
    }

    const handleChange = (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.value
        let change = { ...form, [name]: value }
        setForm(change)
    }

    const handlePasswordSubmit = async (event: React.FormEvent<EventTarget>) => {
        try {
            event.preventDefault();


            await updatePassword(passwordForm)


            setPasswordOpen(false)

        } catch (error: any) {
            console.log(error.data);
        }
    }

    const handlePasswordChange = (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.value
        let change = { ...passwordForm, [name]: value }
        setPasswordForm(change)
    }

    const handleFile = (event: any) => {
        setForm({
            ...form,
            [event.target.name]: event.target.files[0]
        })
    }


    return (
        <>
            {!user && <Navigate to='/login' />}
            <div className="container-fluid my-5 page">
                <div className="row">
                    <div className="col-md-2"></div>
                    <div className="col-md-8 col-12">
                        <div className="row">
                            <div className="col-4">
                                <img src={user?.avatar ? user?.avatar : '/assets/noIcon.png'} className=" avatar w-75" alt="..." />
                                <p className="userProfile">{user?.username}</p>
                            </div>
                            <div className="col-8">
                                <h3>MON PROFIL</h3>
                                <div className="smallLine mb-4"></div>
                                <p>Email : {user?.email}</p>
                                {!updateOpen && !passwordOpen &&
                                    <>
                                        <div className="d-flex modif my-3">
                                            <div onClick={() => setUpdateOpen(true)} className="align-self-center text-center updateProfile">
                                                <i className="fas fa-pencil-alt"></i>
                                            </div>
                                            <p>Modifier mes informations</p>
                                        </div>
                                        <div className="d-flex modif my-3">
                                            <div onClick={() => setPasswordOpen(true)} className="align-self-center text-center updateProfile">
                                                <i className="fas fa-pencil-alt"></i>
                                            </div>
                                            <p>Modifier mon mot de passe</p>
                                        </div>
                                    </>
                                }
                                {updateOpen &&
                                    <form onSubmit={handleSubmit}>
                                        <div className="mb-3">
                                            <label htmlFor="username" className="form-label">Nouveau nom</label>
                                            <input type="text" className="form-control" id="username" name='username' placeholder={user?.username} onChange={handleChange} />
                                        </div>
                                        <div className="mb-3">
                                            <label htmlFor="email" className="form-label">Nouvel email</label>
                                            <input type="email" className="form-control" id="email" name='email' placeholder={user?.email} onChange={handleChange} />
                                        </div>
                                        <div className="mb-3">
                                            <label htmlFor="avatar" className="form-label">Nouvel avatar</label>
                                            <input type="file" className="form-control" id="avatar" name='avatar' alt='img' onChange={handleFile} />
                                        </div>
                                        <div className="mb-3">
                                            <label htmlFor="password" className="form-label">Rentrez votre mot de passe pour confimer</label>
                                            <input type="password" className="form-control" id="password" name='password' autoComplete="off" onChange={handleChange} />
                                        </div>
                                        <div className="d-flex justify-content-center pt-1">
                                            <button type="submit" className="btn btnCustom updateBtn">Valider</button>
                                        </div>
                                    </form>
                                }
                                {passwordOpen &&
                                    <form onSubmit={handlePasswordSubmit}>
                                        <div className="mb-3">
                                            <label htmlFor="oldPassword" className="form-label">Ancien mot de passe</label>
                                            <input type="password" className="form-control" id="oldPassword" name='oldPassword' autoComplete="off" onChange={handlePasswordChange} />
                                        </div>
                                        <div className="mb-3">
                                            <label htmlFor="newPassword" className="form-label">Nouveau mot de passe</label>
                                            <input type="password" className="form-control" id="newPassword" name='newPassword' autoComplete="off" onChange={handlePasswordChange} />
                                        </div>
                                        <div className="d-flex justify-content-center pt-1">
                                            <button type="submit" className="btn btnCustom updateBtn">Valider</button>
                                        </div>
                                    </form>
                                }
                            </div>
                        </div>
                    </div>
                    <div className="col-md-2"></div>
                </div>
                <div className="row my-5">
                    <TeamMain id={Number(user?.teamId)}/>
                </div>
            </div >
        </>
    )
}
