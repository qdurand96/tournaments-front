import { Header } from "../components/Header";

export function MerciPage() {

    return (
        <div className="container-fluid page">
            <div className="row pt-5">
                <Header page='Merci !!!' />
            </div>
        </div>
    )
}